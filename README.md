# Inventory System Frontend

## Last Updated 
11/11/2024 

## Overview
This repository holds the code for the Frontend of the Inventory System for the Bear Necessities Market. With the use of a GUI, employees can take inventory of what is physically available in the market. They are also able to generate reports based on the inventory taken. The Frontend is dependent on the Backend of the Inventory System. 

## Folders 
This repository contains the following folders.

.devcontainer – Contains files for dev container.

.gitlab/issuetemplates - Contains issue templates for Spikes and User Stories.

bin - Contains developer commands.

src - Contains the source code and components for the Frontend. 

tests - Contains tests for system functionality with additional folders for manual tests and automatic test-runner scripts. 

docs/developer - Contains files that reflect current state of system and should be useful to developers working on the project.

## Status 
The Frontend portion of the MVP is complete, and Integration's MVP is also completed, however, there is not a working version deployed currently. 

## Setting up the Dev Environment

```bash
# Clone the project
$ git clone https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/frontend.git

# Change directory to the project
$ cd frontend

# Initial build
$ cd bin
$ sh build.sh

# Intended run of the project
$ cd ../src
$ docker-compose up
```

## Running the Application
Currently, user input and output are performed through a GUI hosted on port 10300. Connect to localhost:10300 to interact with the system.

## Development Infrastructure
The following tools are used or intended to be used in the frontend of the Inventory Systems:
- [Vue](https://vuejs.org/)
- [JavaScript](https://www.javascript.com/)
- [Axios](https://axios-http.com/)
- [Express](https://expressjs.com/)
- [Node.js](https://nodejs.org/en/)

## License
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)


