#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd "$SCRIPT_DIR/.."

docker compose -f "${SCRIPT_DIR}"/../src/docker-compose.yaml down

PROJECT_NAME="$( basename "$( pwd )" )"
docker rmi -f frontend:latest
docker build -t "$PROJECT_NAME" -f "${SCRIPT_DIR}/../src/Dockerfile" . 

docker compose -f "${SCRIPT_DIR}"/../src/docker-compose.yaml up

