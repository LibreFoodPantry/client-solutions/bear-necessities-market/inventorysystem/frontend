#!/usr/bin/env bash
DOCKERFILE_PATH="./tests/test-runner/Dockerfile"

docker container prune -f >/dev/null 2>&1
docker-compose -f "$DOCKERFILE_PATH" up -d --remove-orphans >/dev/null 2>&1
sleep 10
