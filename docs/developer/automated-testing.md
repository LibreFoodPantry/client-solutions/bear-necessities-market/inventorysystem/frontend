# Testing

## Automated Testing
As of right now, automated testing has not been fully integrated to the InventoryFrontend system.

## Manual Testing
Tests can be run manually in the **calls.http** file in the **tests/manual/** */ directory. The following tests are avaiable:
- GET Version
- GET all Inventory rows
- GET Iventory row by ID
- POST row to Inventory
- PUT update row Inventoy with updated field
- DELETE entory from Inventory by ID
- POST multiple rows to Entries
- GET all Entries rows 
- GET Entries with invalid id 
- POST row to Reports
- Get all Reports rows
- Get Reports row by date field
- DELETE all rows in Reports

