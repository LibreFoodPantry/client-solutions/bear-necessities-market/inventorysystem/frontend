# Continuous Integration
Continuous Integration, also known as CI, covers building, linting and integration. Having CI helps integrate new code to the project by automatically building and testing it.

## .gitlab_ci.yml
The [.gitlab_ci.yaml](../../.gitlab-ci.yml) contains a link to the pipeline as well as _bulid.sh_ and _test.sh_ commands. For more information visit the offical .gitlab_ci.yml [documentation](https://docs.gitlab.com/ee/ci/yaml/).