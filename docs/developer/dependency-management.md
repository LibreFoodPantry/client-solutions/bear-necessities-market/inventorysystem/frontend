# Dependency Managment
Having a dependency management system helps manage the versions of all the tools and libraries used during development, testing,deployment, and running of the application. This allows for anyone to reliably recreate the same enviroment, regardless of machine. A dependency management systems allows the ability to install libraries and tools at a specific version, while tracking said library and tools with their verison numbers. This allows to see which libraries/tools are out of date and/or contain vulnerablities. 

## npm
The NodeJS package manager, npm, is the main handler of Javascript dependency management in InventorySystemFrontend.
For more information visit the npm [official documentation](https://docs.npmjs.com/).

## Installing a Dependency with npm
To install a packaged name ```thispackage`` do:

```bash
npm install thispackage
```

Should 'thispackage' be a **development dependency**, ie only need during development, add the ```--save-dev tag```:

```bash
npm install thispackage --save-dev
```

Should a specific version of ```thispackage``` is needed, specify the version:

```bash
npm install thispackage@1.2.3
```

## Updating Dependencies
As of right now, updating dependencies is done manually. When updating, it is important to understand the [semantic versioning](https://docs.npmjs.com/about-semantic-versioning) to make sure no break changes could be introduced to the source code.

## Docker
Docker has no automatic dependency management tool. All dependencies must be manually installed. It is import to also install specifc dependency versions to maintain consistency.