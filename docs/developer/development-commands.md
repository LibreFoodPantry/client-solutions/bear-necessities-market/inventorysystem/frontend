# Developer Commands
Developer commands can be found in the **bin** directory.

## Build
Generates a Docker image of the InventorySystemFrontend.\
**This command should only be used if no Docker image exists or changes have been done to the source code.**

```bash
./bin/build.sh
```

## Up
Generates a InventorySystemFrontend server running locally, along with a MongoDB database, InventorySystemBackend, and RabbitMQ server. The servers will continue to run and log messages until they are terminated with ``Ctrl+C```.

```bash
./bin/up.sh
```

## Down
Removes all Docker containers and networks. \
**This will also bring down all servers if run in a seperate terminal tab.**

```bash
./bin/down.sh
```

## Rebuild
Takes down any currently running Docker containers and networks, and rebuilds the InventorySystemFrontend Docker image, while also booting up the other Docker containers. These container will run until terminated with ```Ctrl+C```. \
**This command should be used for when changes are made to the *src* directory to be observable.**

```bash
./bin/rebuild.sh
```
## Restart
Brings down any running Docker containers and networks, and then proceeds to restart them. These container will run until terminated with ```Ctrl+C```. \
**This command should be used to restart the container for whatever reason.**\
Note: this will *not* allow for changes in the *src* directory to be observable.

```bash
./bin/restart.sh
```

## Pre-Merge Squash
Squashes all commits of the current feature branch into to one single commit to allow for one convention commit. \
**This has to be used before creating a merge request for the current feature branch**

```bash
./bin/premerge-squash.sh
```