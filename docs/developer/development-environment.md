# Development Environment
As of recent annoucnemts by GitPod, and their new project [GitPod Flex](https://www.gitpod.io/blog/introducing-gitpod-flex). The decision has been made to make the migration to **VS Code Dev Containers** for development.

## VS Code Dev Container
VSCode Dev Containers allow for developers to work on a code base with the same dependcies, files, environment, etc. This uniform environment negates the "it works on my machine" problem.

## Getting Started With Dev Containers
The following items must be installed by developers onto their local machine:
- VS Code
- VS Code Dev Container Extension
- Docker Desktop
- Git

## Downloading Necessary Dependencies
### VS Code
To install VS Code, visit the official [download page](https://code.visualstudio.com/Download), and install the appropriate version for your operating system.

### VS Code Dev Containers
To install the VS Code Dev Containers extension, Open VS Code, press either Ctrl+Shift+X (Cmd+Shift+X on Mac) or press the square icon in the nav menu, search for Dev Containers, and press install.

### Docker Desktop
To install Docker Desktop, visit the official [download page](https://www.docker.com/products/docker-desktop/), and install the appropriate version for your operating system.

### Git
To install Git, visit the official [download page](https://git-scm.com/), and install the appropriate version for your operating system.\
Note: If you are on a MacOS machine, Git should be installed by default. To check use:

```bash
git --version
```

## Cloning Project
To clone InventorySystemFrontend, simply go to the InventorySystemFrontend [homepage](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/frontend). Then press the ```Code``` button right below the project title. Then either press ```Visual Studio Code (HTTPS)```, this will prompt you to find a place to save the repository in your directory before opening the clone in VSCode, or copy the HTTPs address via ```Clone with HTTPS```, this will require you to use the command ```git clone [address]``` in whatever directory you want to clone the reposiotory in.

## Reopening
The project must be then reopened in a Dev Container to ensure indentical development environment. VS Code should prompt you to reopen when the project is first opened. If not, press the blue lighting button in the bottm left corner.

## .devcontainer
The .devcontainer directory holds the necessary dependencies needed when created the Dev Container. Should there be any new dependencies needed in the environment, they should be declared in the ```devcontainer.json``` file. \
For more information on Dev Container and their configuration, see the official [documentation](https://code.visualstudio.com/docs/remote/create-dev-container).