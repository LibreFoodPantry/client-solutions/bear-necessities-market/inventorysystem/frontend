# Documentation
As of right now, docs/developer contains the only documentation in InventorySystemFrontend

## docs/developer
The docs/developer directory holds "cheat sheets" for different aspects of development written in Markdown. Markdown will be automatically rendered on GitLab. To render locally use the command ```Ctrl+Shift+P``` or ```Cmd+Shift+P``` on Mac.