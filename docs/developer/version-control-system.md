# Version Control System
To mantain version control, [Git](https://git-scm.com/) is used. The following document describes its configuration, and how Git commit messages are formatted.

## .gitattributes
.gitattrbutes file ensures that all line endings are normalized throughout the project. Developers with different operation systems could lead to conflicting line endings without a .gitattributes file. \
For more information on .gitattributes visit Git's documentation on [.gitattributes](https://git-scm.com/docs/gitattributes). \
\
 Note: .gitattributes files should not be edited directly, but rather concatenated with templates from [alexkaratarakis/gitattributes](https://github.com/alexkaratarakis/gitattributes), a repository of template .gitattributes files.

 ## .gitignore
 .gitignore file ensures only source code is stored in the projects repository. Items such as generated files and deployment specifc data should be ignored. For more information visit Git's documentation on [.gitignore](https://git-scm.com/docs/gitignore)\
 \
 Note: .gitignore files should not edited directly, but rather concatenated with [github/gitignore](https://github.com/github/gitignore), a repo with common .gitignore values for most desired languages.

 ## Conventional Commits
[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) are the desired format for when writing commit messages. For more examples of Convention Commits visit [How to Write Better Git Commit Messages](https://www.freecodecamp.org/news/how-to-write-better-git-commit-messages/).


