import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Inventory from '../views/Inventory.vue'
import InventorySession from '../views/InventorySession.vue'
import Reports from '../views/Reports.vue'
import ViewReport from '../views/ViewReport.vue'

const routes = [
  {path:'/',name:'Home',component:Home},
  {path:'/inventory',name:'Inventory',component:Inventory},
  {path:'/session',name:'InventorySession',component:InventorySession},
  {path:'/reports',name:'Reports',component:Reports},
  {path:'/viewReport/:date',name:'ViewReport',component:ViewReport}
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router;